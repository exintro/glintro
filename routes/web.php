<?php

use App\Models\Menu;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LandController;
use App\Http\Controllers\PageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [LandController::class,'index'])->name('home');

Route::get('/pages/expirience', [PageController::class,'expirience'])->name('expirience');

Route::get('/pages/hobbies', [PageController::class,'hobbies'])->name('hobbies');

Route::get('/pages/issues', [PageController::class,'issues'])->name('issues');

