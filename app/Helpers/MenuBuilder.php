<?php
namespace App\Helpers;

class MenuBuilder
{
    public string $label;

    public string $slug;

    public int $pageId;

    public string $pageTitle;

    public function __construct(
        string $label,
        string $slug,
        int $pageId,
        string $pageTitle,
    ){
        $this->label = $label;
        $this->slug = $slug;
        $this->pageId = $pageId;
        $this->pageTitle = $pageTitle;
    }
}
