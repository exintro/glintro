<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function expirience()
    {
        return view('pages.expirience', [
            'menus'=>  $this->menuBuilder
        ]);
    }
    public function hobbies()
    {
        return view('pages.hobbies', [
            'menus'=>  $this->menuBuilder
        ]);
    }

    public function issues()
    {
        return view('pages.issues', [
            'menus'=>  $this->menuBuilder
        ]);
    }
}
