<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Helpers\MenuBuilder;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    protected array $menuBuilder = [];

    public function __construct()
    {
        $menus = Menu::all();

        $date = new \DateTime('now');
        foreach ($menus as $menu)
        {

            $page = DB::table('pages')->where('id', $menu->page_id)->first();

            if($date > $page->published)
            {
                array_push($this->menuBuilder, new MenuBuilder(
                    $menu->label,
                    $page->slug,
                    $menu->page_id,
                    $page->title
                ));
            }

        }
    }
}
