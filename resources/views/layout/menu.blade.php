<div class="container none text-gray-50">Grega Lipovšček laravel exercise</div>

<div class="container w-full dark:bg-gray-900">
    <nav class="w-full  dark:bg-gray-900">
        <div class="w-full flex flex-wrap items-center justify-between mx-auto p-4">
          <div class="hidden w-full md:block md:w-auto" id="navbar-default">
            <ul class="font-medium flex flex-col p-4 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 rtl:space-x-reverse md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
                <a href="/" class="block py-2 px-3 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white md:dark:text-blue-500" aria-current="page">Home</a>
                @foreach  ($menus as $menu)
                    <li>
                        <a href="/pages/{{$menu->slug}}" class="block py-2 px-3 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white md:dark:text-blue-500" aria-current="page">{{ucfirst($menu->label)}}</a>
                    <li>
                @endforeach
                <a href="https://gitlab.com/exintro/glintro.git" target="new" class="block py-2 px-3 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white md:dark:text-blue-500" aria-current="page">Project git link</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
</div>

