@extends('layout.themplate')

@section('content')

<div class="grid grid-cols-2 md:grid-cols-3 gap-4 m-11">
    <div>
        <img class="h-auto max-w-full rounded-lg" src="/img/srsLogin.jpg" alt="">
    </div>
    <div>
        <img class="h-auto max-w-full rounded-lg" src="/img/wvoLand.jpg" alt="">
    </div>
    <div>
        <img class="h-auto max-w-full rounded-lg" src="/img/tem.jpg" alt="">
    </div>
    <div>
        <img class="h-auto max-w-full rounded-lg" src="/img/deliveryNote.jpg" alt="">
    </div>
    <div>
        <img class="h-auto max-w-full rounded-lg" src="/img/dinerSignatures.jpg" alt="">
    </div>
    <div>
        <img class="h-auto max-w-full rounded-lg" src="/img/invoiceExample.jpg" alt="">
    </div>
</div>

@endsection
