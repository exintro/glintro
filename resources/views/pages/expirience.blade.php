@extends('layout.themplate')

@section('content')

<div class="container box-contentn m-auto">
    <div class="text-4xl font-extrabold mt-1.5">Expirieance</div>

    <div>
        I got to know programming for the first time during the first internet-ralay-chats(IRC) and later at the Faculty of Computer Science and Informatics.
        We started with java, and programming in php was my hobby.
        In 2009, I got the idea of digital production processes and started building software frameworks.
        <br /><br />
        the programs were initially made in php 5,
        javascript and mariadb database and they supported two-factor authentication.
        Frameworks were in their infancy and javascript did not yet have object orianted programming.
        Therefore, the resulting programs had thousands of lines but this work was like a hobby and
        that's why I'm adding some pictures and documents in the hobbies section.
        <br /><br />
        The first program was an online accountant. I issued an invoice online for the first time in 2013. Later, I created the e-fakture.eu portal, where customers could receive invoices via a browser. It supported the import of obsv forms from the tax office, the creation of travel orders and the issuance of the xml annual report.
        <br /><br />
        I was setting up a virtual online office at the same time.
        It was needed as an admin control panel, and at the same time I made a schedule editor for the client.
        He supported the creation of schedules, based on statistics, employee wishes and orders.
        The program was made modular, so I could quickly add different modules,
        such as F&B manager, issuing and importing delivery notes, and the digital haccp system. ( controller programming )
        <br /><br />
        The last project is a programming framework for microservice application service with costume identity provider.
    </div>

</div>

@endsection
