@extends('layout.themplate')

@section('content')

<div class="logo-frame">

    <div class="upper-logo">
        <div class="n-row s-smal">0 1 0 1 0 1 0 1 0 1 0 0 1 0 1 0 1 0 0 1 0 1 0 1 0</div>
        <div class="n-row s-big">0 1 0 1 0 1 0 1 0 1 0</div>
        <div class="n-row s-smal">0 1 0 1 0 1 0 1 0 1 0 0 1 0 1 0 1 0</div>
        <div class="n-row">0 1 0 1 0 1 0 1 0 1</div>
        <div class="n-row s-smal">0 1 0 1 0 1 0 1 0 1 0</div>
        <div class="n-row s-smal">0 1 0 1 0 1 0 1 0 1 0 1</div>
        <div class="n-row">0 1 0 1 0 1 0 1 0 1 0</div>
        <div class="n-row s-smal">0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1</div>
        <div class="n-row">0 1 0 1 0 1 0 0 1 0 1 0</div>
    </div>

    <div class="bottom-logo">
        <div class="cy-frame">
            <div class="cy-crt ct150 bsh"></div>
        </div>
        <div class="cy-frame">
            <div class="cy-crt ct60 bsh"></div>
        </div>
        <div class="cy-frame">
            <div class="cy-crt bsh"></div>
        </div>
        <div class="cy-frame">
            <div class="cy-crt cl60 bsh"></div>
        </div>
        <div class="cy-frame">
            <div class="cy-crt cl150 bsh"></div>
        </div>
    </div>
    <div class="text-logo">
        Grega Lipovšček
    </div>

</div>

@endsection
